// payload = { 
//     OtherTaxOnVat : true,
//     billDiscountPercentage : 20,
//     billDiscountCash : 0,
//     roundingNearestValue : 100,
//     roundingMode : "AUTO",
//     sales : [
//         {
//             qty : 1,
//             inclusivePrice : 33000,
//             menuDiscountPercentage : 0,
//             menuDiscountCash : 0,
//             otherTaxPercentage : 0,
//             taxPercentage : 10,
//             proRateBillDisc : true
//         },
//         {
//             qty : 1,
//             inclusivePrice : 9000,
//             menuDiscountPercentage : 0,
//             menuDiscountCash : 0,
//             otherTaxPercentage : 0,
//             taxPercentage : 10,
//             proRateBillDisc : true
//         }
//     ]
// }
function inclusiveAfterDiscount(payload) {
    //@params
    let OtherTaxOnVat = payload.OtherTaxOnVat;
    let billDiscountPercentage = payload.billDiscountPercentage;
    let billDiscountCash = payload.billDiscountCash;
    let roundingNearestValue = payload.roundingNearestValue;
    let roundingMode = payload.roundingMode;

    // @counting total
    let allSalesInclusivePriceTotal = 0;
    for (let i = 0; i < payload.sales.length; i++) {
        let sale = payload.sales[i];
        if (sale.proRateBillDisc && (sale.inclusivePrice > 0)) {
            let inclusivePriceTotal = sale.inclusivePrice * sale.qty;
            allSalesInclusivePriceTotal += inclusivePriceTotal;
        }
    }

    // @counting reward total
    let allSalesInclusivePriceOfRewardTotal = 0;
    for (let i = 0; i < payload.sales.length; i++) {
        let saleMinus = payload.sales[i];
        if (saleMinus.inclusivePrice < 0) {
            let inclusivePriceTotal = saleMinus.inclusivePrice * saleMinus.qty;
            allSalesInclusivePriceOfRewardTotal += inclusivePriceTotal;
        }
    }

    // BEGIN CALCULATION
    //@header inclusiveDiscountTotal
    let inclusiveDiscountTotal = 0;
    if (billDiscountPercentage == 0) {
        inclusiveDiscountTotal = billDiscountCash;
    } else {
        inclusiveDiscountTotal = allSalesInclusivePriceTotal * (billDiscountPercentage / 100);
    }
    //@header sales
    let sales = [];
    for (let i = 0; i < payload.sales.length; i++) {
        let sale = payload.sales[i];

        //@inclusivePriceTotal
        let inclusivePriceTotal = sale.inclusivePrice * sale.qty;
        //@inclusiveDiscountTotalValue
        let inclusiveDiscountTotalValue = 0;
        if (inclusivePriceTotal > 0) {
            if (sale.menuDiscountPercentage == 0) {
                if ((sale.menuDiscountCash * sale.qty) > inclusivePriceTotal) {
                    inclusiveDiscountTotalValue = inclusivePriceTotal;
                } else {
                    inclusiveDiscountTotalValue = sale.menuDiscountCash * sale.qty;
                }
            } else {
                inclusiveDiscountTotalValue = (inclusivePriceTotal * sale.menuDiscountPercentage) / 100;
            }
        }
        //@inclusiveBillDiscountTotalPerMenu
        let inclusiveBillDiscountTotalPerMenu = 0;
        if (inclusivePriceTotal > 0) {
            let rewardProrate = 0;
            if (billDiscountPercentage > 0) {
                rewardProrate = (allSalesInclusivePriceOfRewardTotal * -1) * (inclusivePriceTotal / allSalesInclusivePriceTotal);
            }
            inclusiveBillDiscountTotalPerMenu = sale.proRateBillDisc ? inclusiveDiscountTotal * ((inclusivePriceTotal - rewardProrate) / allSalesInclusivePriceTotal) : 0;
            if (isNaN(inclusiveBillDiscountTotalPerMenu)) {
                inclusiveBillDiscountTotalPerMenu = 0;
            }
        }
        //@inclusivePriceTotalAfterDisc
        let inclusivePriceTotalAfterDisc = 0;
        if (inclusivePriceTotal > 0) {
            if ((inclusivePriceTotal - inclusiveBillDiscountTotalPerMenu - inclusiveDiscountTotalValue) > 0) {
                inclusivePriceTotalAfterDisc = inclusivePriceTotal - inclusiveBillDiscountTotalPerMenu - inclusiveDiscountTotalValue;
            }
        } else {
            inclusivePriceTotalAfterDisc = inclusivePriceTotal;
        }
        //@price
        let price = 0;
        if (OtherTaxOnVat) {
            price = ((sale.inclusivePrice * 100) / (100 + sale.taxPercentage)) * (100 / (100 + sale.otherTaxPercentage));
        } else {
            price = (sale.inclusivePrice * 100) / (100 + sale.taxPercentage + sale.otherTaxPercentage);
        }
        //@priceTotal
        let priceTotal = price * sale.qty;
        //@priceTotalAfterDisc
        let priceTotalAfterDisc = 0;
        if (OtherTaxOnVat) {
            priceTotalAfterDisc = ((inclusivePriceTotalAfterDisc * 100) / (100 + sale.taxPercentage)) * (100 / (100 + sale.otherTaxPercentage));
        } else {
            priceTotalAfterDisc = (inclusivePriceTotalAfterDisc * 100) / (100 + sale.taxPercentage + sale.otherTaxPercentage);
        }
        //@discountCash
        let discountCash = 0;
        if (OtherTaxOnVat) {
            discountCash = ((sale.menuDiscountCash * 100) / (100 + sale.taxPercentage)) * 100 / (100 + sale.otherTaxPercentage);
        } else {
            discountCash = (sale.menuDiscountCash * 100) / (100 + sale.taxPercentage + sale.otherTaxPercentage);
        }
        // @discountTotalValue
        let discountTotalValue = 0;
        if (inclusivePriceTotal > 0) {
            if (sale.menuDiscountCash > 0) {
                if ((discountCash * sale.qty) > priceTotal) {
                    discountTotalValue = priceTotal;
                } else {
                    discountTotalValue = discountCash * sale.qty;
                }

            } else {
                discountTotalValue = (priceTotal * sale.menuDiscountPercentage) / 100;
            }
        }
        // @otherTaxTotal
        let otherTaxTotal = (sale.otherTaxPercentage * priceTotalAfterDisc) / 100;
        // @taxTotal
        let taxTotal = 0;
        if (OtherTaxOnVat) {
            taxTotal = (priceTotalAfterDisc + otherTaxTotal) * (sale.taxPercentage / 100);
        } else {
            taxTotal = priceTotalAfterDisc * (sale.taxPercentage / 100);
        }
        //@billDiscountPerMenu
        let billDiscountPerMenu = 0;
        if (inclusivePriceTotal > 0) {
            if ((priceTotal - discountTotalValue - priceTotalAfterDisc) > 0) {
                billDiscountPerMenu = priceTotal - discountTotalValue - priceTotalAfterDisc
            }
        }

        let saleShelter = {
            qty: sale.qty,
            inclusiveBillDiscountTotalPerMenu: inclusiveBillDiscountTotalPerMenu, //- Bill Discount Per Menu
            billDiscountPerMenu: billDiscountPerMenu, //- Final Bill Disc per Menu
            inclusiveDiscountTotalValue: inclusiveDiscountTotalValue, //- Inc. Discount Value
            inclusiveDiscountValue: inclusiveDiscountTotalValue / sale.qty,
            inclusivePrice: sale.inclusivePrice,
            inclusivePriceAfterDisc: inclusivePriceTotalAfterDisc / sale.qty,
            inclusivePriceTotal: inclusivePriceTotal, //- Inclusive Total
            inclusivePriceTotalAfterDisc: inclusivePriceTotalAfterDisc, //- Total After Bill Discount
            price: price, //- Price
            priceAfterDisc: priceTotalAfterDisc / sale.qty,
            priceTotal: priceTotal, //spreadsheets - Menu Subtotal Before Disc
            priceTotalAfterDisc: priceTotalAfterDisc, //- Menu Subtotal
            discountCash: discountCash, //- Non Inclusive Disc RP
            discountValue: discountTotalValue / sale.qty,
            discountTotalValue: discountTotalValue, //- Menu Discount Value
            otherTax: otherTaxTotal / sale.qty,
            otherTaxTotal: otherTaxTotal, //- Other Tax Value
            tax: taxTotal / sale.qty,
            taxTotal: taxTotal, //- Tax Value,
            //additionalInformation ->
            id: sale.id,
            salesNum: sale.salesNum,
            menuRefID: sale.menuRefID,
            menuGroupID: sale.menuGroupID,
            //
            taxPercentage: sale.taxPercentage
        }

        sales.push(saleShelter);
    }

    // BEGIN CALCULATION HEAD

    //@head-variable
    let subtotalHead = 0;
    let inclusiveDiscountTotalHead = 0;
    let discountTotalHead = 0;
    let menuDiscountTotalHead = 0;
    let inclusiveMenuDiscountTotalHead = 0;
    let otherTaxTotalHead = 0;
    let taxTotalHead = 0;
    let pb1TotalHead = 0;
    let ppnTotalHead = 0;
    let grandTotalHead = 0;
    let roundingTotalHead = 0;
    let grandTotalAfterRoundingHead = 0;
    for (let i = 0; i < sales.length; i++) {
        let sale = sales[i];
        subtotalHead += sale.priceTotal;
        inclusiveDiscountTotalHead += sale.inclusiveBillDiscountTotalPerMenu;
        discountTotalHead += sale.billDiscountPerMenu;
        menuDiscountTotalHead += sale.discountTotalValue;
        inclusiveMenuDiscountTotalHead += sale.inclusiveDiscountTotalValue
        otherTaxTotalHead += sale.otherTaxTotal;
        taxTotalHead += sale.taxTotal;
        if (sale.taxPercentage == 10) {
            pb1TotalHead += sale.taxTotal;
        }
        if (sale.taxPercentage == 11) {
            ppnTotalHead += sale.taxTotal;
        }
    }

    if ((subtotalHead - menuDiscountTotalHead - discountTotalHead + otherTaxTotalHead + taxTotalHead) > 0) {
        grandTotalHead = subtotalHead - menuDiscountTotalHead - discountTotalHead + otherTaxTotalHead + taxTotalHead;
    }

    if (roundingMode == "UP") {
        roundingTotalHead = grandTotalHead - Math.ceil(grandTotalHead / roundingNearestValue) * roundingNearestValue;
    } else if (roundingMode == "DOWN") {
        roundingTotalHead = grandTotalHead - Math.floor(grandTotalHead / roundingNearestValue) * roundingNearestValue;
    } else {
        roundingTotalHead = grandTotalHead - Math.round(grandTotalHead / roundingNearestValue) * roundingNearestValue;
    }

    grandTotalAfterRoundingHead = grandTotalHead - roundingTotalHead;

    let result = {
        subtotal: subtotalHead,
        inclusiveDiscountTotal: inclusiveDiscountTotalHead,
        discountTotal: discountTotalHead,
        inclusiveMenuDiscountTotal: inclusiveMenuDiscountTotalHead,
        menuDiscountTotal: menuDiscountTotalHead,
        otherTaxTotal: otherTaxTotalHead,
        taxTotal: taxTotalHead,
        pb1Total: pb1TotalHead,
        ppnTotal: ppnTotalHead,
        grandTotal: grandTotalHead,
        roundingTotal: roundingTotalHead,
        grandTotalAfterRounding: grandTotalAfterRoundingHead,
        sales: sales,
    };
    return result;
}